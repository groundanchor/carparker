package io.groundanchor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class App {

  public static void main(String[] args) {
    CarParker carParker = new CarParker();
    List<Space> carPark = carParker.addSpace();
    Services services = new ServiceImpl();

//    System.out.println(services.isAvailable(carPark.get(0), (Space s) -> s.isBooked()));
    System.out.println(services.isAvailable(carPark.get(0), Space::isBooked));
    Supplier<ArrayList<String>> myArrayList = ArrayList::new;
    ArrayList<String> l1 = myArrayList.get();
    l1.add("Stuart");
    Consumer<String> s1 = System.out::println;
    s1.accept(l1.get(0));
    var map = new HashMap<Integer, String>();
    BiConsumer<Integer, String> bc1 = map::put;
    bc1.accept(1, "Stuart");
    bc1.accept(2, "Alan");
    System.out.println(map);
  }

}
