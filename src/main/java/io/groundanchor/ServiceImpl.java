package io.groundanchor;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class ServiceImpl implements Services{
  @Override
  public boolean isAvailable(Space space, Predicate predicate) {
    return predicate.test(space);
  }

//  @Override
//  public List<Space> listAllAvailable(List<Space> allSpaces, Supplier<Space> spaceTester) {
//    return null;
//  }
}
