package io.groundanchor;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

public interface Services {
  boolean isAvailable(Space space, Predicate<Space> predicate);
//  List<Space> listAllAvailable(List<Space> allSpaces, Supplier<List<Space>> spaceTester);
}
