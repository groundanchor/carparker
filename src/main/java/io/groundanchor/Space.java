package io.groundanchor;

public class Space {
  boolean booked;
  String owner;
  int id;

  public Space(boolean booked, String owner, int id) {
    this.booked = booked;
    this.owner = owner;
    this.id = id;
  }

  public boolean isBooked() {
    return booked;
  }
}
